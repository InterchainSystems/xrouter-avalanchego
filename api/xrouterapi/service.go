// (c) 2019-2020, Ava Labs, Inc. All rights reserved.
// See the file LICENSE for licensing terms.
package xrouterapi

import (
	"net/http"
	"strings"

	"github.com/ava-labs/avalanchego/utils/json"
	"github.com/ava-labs/avalanchego/utils/logging"
	"github.com/blocknetdx/go-xrouter/xrouter"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/gorilla/rpc/v2"

	"github.com/ava-labs/avalanchego/snow/engine/common"
)

// XRouter is the API service for unprivileged info on a node
type XRouterService struct {
	log        logging.Logger
	config     chaincfg.Params
	client     *xrouter.Client
	queryCount int
}

// NewService returns a new XRouter API service
func NewService(log logging.Logger, config chaincfg.Params, client *xrouter.Client, queryCount int) (*common.HTTPHandler, error) {
	newServer := rpc.NewServer()
	codec := json.NewCodec()
	newServer.RegisterCodec(codec, "application/json")
	newServer.RegisterCodec(codec, "application/json;charset=UTF-8")
	if err := newServer.RegisterService(&XRouterService{
		log:        log,
		config:     config,
		client:     client,
		queryCount: queryCount,
	}, "xrouterapi"); err != nil {
		return nil, err
	}
	return &common.HTTPHandler{Handler: newServer}, nil
}

// GetNetworkServices
type GetNetworkServicesReply struct {
	NetworkServices []string `json:"networkServices"`
}

// GetNetworkServices
func (service *XRouterService) GetNetworkServices(_ *http.Request, _ *struct{}, reply *GetNetworkServicesReply) error {
	service.log.Info("XRouter: GetNetworkServices called")

	xrouterReply := service.client.ListNetworkServices()
	reply.NetworkServices = xrouterReply

	return nil
}

// // UpdateNetworkServices
// type UpdateNetworkServicesArgs struct {
// 	NodeCount string `json:"node_count"`
// }

// type UpdateNetworkServicesReply struct {
// 	IsUpdated string `json:"isUpdated"`
// }

// // UpdateNetworkServices
// func (service *XRouterService) UpdateNetworkServices(_ *http.Request, args *UpdateNetworkServicesArgs, reply *UpdateNetworkServicesReply) error {
// 	service.log.Info("XRouter: UpdateNetworkServices called")
// 	if xrouterReply, err := service.client.UpdateNetworkServices(args.NodeCount, service.queryCount); err != nil {
// 		service.log.Fatal("error: %v", err)
// 		return err
// 	} else {
// 		reply.IsUpdated =  string(xrouterReply.Reply)
// 	}
// 	return nil
// }

// // ConnectedNodes
// type ConnectedNodesReply struct {
// 	ConnectedNodes string `json:"connectedNodes"`
// }

// // ConnectedNodes
// func (service *XRouterService) ConnectedNodes(_ *http.Request, _ *struct{}, reply *ConnectedNodesReply) error {
// 	service.log.Info("XRouter: ConnectedNodes called")
// 	if xrouterReply, err := service.client.ConnectedNodes(); err != nil {
// 		service.log.Fatal("error: %v", err)
// 		return err
// 	} else {
// 		reply.ConnectedNodes =  string(xrouterReply.Reply)
// 	}
// 	return nil
// }

// GetTransaction
type GetTransactionArgs struct {
	Blockchain string `json:"blockchain"`
	ID         string `json:"tx_id"`
	NodeCount  int    `json:"node_count"`
}

type GetTransactionReply struct {
	Transaction string `json:"transaction"`
}

// GetTransaction
func (service *XRouterService) GetTransaction(_ *http.Request, args *GetTransactionArgs, reply *GetTransactionReply) error {
	service.log.Info("XRouter: GetTransaction called")
	if xrouterReply, err := service.client.GetTransaction(args.Blockchain, args.ID, args.NodeCount); err != nil {
		service.log.Fatal("error: %v", err)
		return err
	} else {
		reply.Transaction = string(xrouterReply.Reply)
	}
	return nil
}

// GetTransactions
type GetTransactionsArgs struct {
	Blockchain string `json:"blockchain"`
	IDS        string `json:"tx_ids"`
	NodeCount  int    `json:"node_count"`
}

type GetTransactionsReply struct {
	Transactions string `json:"transactions"`
}

// GetTransactions
func (service *XRouterService) GetTransactions(_ *http.Request, args *GetTransactionsArgs, reply *GetTransactionsReply) error {
	service.log.Info("XRouter: GetTransactions called")
	s := strings.Split(args.IDS, ",")
	var params []interface{}
	for i := range s {
		params = append(params, s[i])
		service.log.Info("ID-%v: %s", i, s[i])
	}
	if xrouterReply, err := service.client.GetTransactions(args.Blockchain, params, args.NodeCount); err != nil {
		service.log.Fatal("error: %v", err)
		return err
	} else {
		reply.Transactions = string(xrouterReply.Reply)
	}
	return nil
}

// GetBlocks
type GetBlocksArgs struct {
	Blockchain string `json:"blockchain"`
	IDS        string `json:"block_ids"`
	NodeCount  int    `json:"node_count"`
}

type GetBlocksReply struct {
	SnodeReply xrouter.SnodeReply `json:"snodeReply"`
}

// GetBlocks
func (service *XRouterService) GetBlocks(_ *http.Request, args *GetBlocksArgs, reply *GetBlocksReply) error {
	service.log.Info("XRouter: GetBlocks called")
	s := strings.Split(args.IDS, ",")
	var params []interface{}
	for i := range s {
		params = append(params, s[i])
		service.log.Info("BLOCKS-%v: %s", i, s[i])
	}
	if xrouterReply, err := service.client.GetBlocks(args.Blockchain, params, args.NodeCount); err != nil {
		service.log.Fatal("error: %v", err)
		return err
	} else {
		reply.SnodeReply = xrouterReply
	}
	return nil
}

// GetBlocksRaw
type GetBlocksRawArgs struct {
	Blockchain string `json:"blockchain"`
	IDS        string `json:"block_ids"`
	NodeCount  int    `json:"node_count"`
}

type GetBlocksRawReply struct {
	Blockhash string               `json:"blockhash"`
	NodeArray []xrouter.SnodeReply `json:"nodeArray"`
}

// GetBlocksRaw
func (service *XRouterService) GetBlocksRaw(_ *http.Request, args *GetBlocksRawArgs, reply *GetBlocksRawReply) error {
	service.log.Info("XRouter: GetBlocksRaw called")
	s := strings.Split(args.IDS, ",")
	var params []interface{}
	for i := range s {
		params = append(params, s[i])
		service.log.Info("BLOCKS-%v: %s", i, s[i])
	}
	if blockData, xrouterReply, err := service.client.GetBlocksRaw(args.Blockchain, params, args.NodeCount); err != nil {
		service.log.Fatal("error: %v", err)
		return err
	} else {
		reply.Blockhash = blockData
		reply.NodeArray = xrouterReply
	}
	return nil
}

// GetBlockCountRaw
type GetBlockCountRawArgs struct {
	Blockchain string `json:"blockchain"`
	NodeCount  int    `json:"node_count"`
}

type GetBlockCountRawReply struct {
	NodeCount string               `json:"node_count"`
	NodeArray []xrouter.SnodeReply `json:"nodeArray"`
}

// GetBlockCountRaw
func (service *XRouterService) GetBlockCountRaw(_ *http.Request, args *GetBlockCountRawArgs, reply *GetBlockCountRawReply) error {
	service.log.Info("XRouter: GetBlockCountRaw called")
	if nodeCount, snodeReplies, err := service.client.GetBlockCountRaw(args.Blockchain, args.NodeCount); err != nil {
		service.log.Fatal("error: %v", err)
		return err
	} else {
		reply.NodeCount = nodeCount
		reply.NodeArray = snodeReplies
	}
	return nil
}

// GetBlockCount
type GetBlockCountArgs struct {
	Blockchain string `json:"blockchain"`
	NodeCount  int    `json:"node_count"`
}

type GetBlockCountReply struct {
	SnodeReply xrouter.SnodeReply `json:"snodeReply"`
}

// GetBlockCount
func (service *XRouterService) GetBlockCount(_ *http.Request, args *GetBlockCountArgs, reply *GetBlockCountReply) error {
	service.log.Info("XRouter: GetBlockCount called")
	if snodeReply, err := service.client.GetBlockCount(args.Blockchain, args.NodeCount); err != nil {
		service.log.Fatal("error: %v", err)
		return err
	} else {
		reply.SnodeReply = snodeReply
	}
	return nil
}

// GetBlockHashRaw
type GetBlockHashRawArgs struct {
	Blockchain string      `json:"blockchain"`
	Block      interface{} `json:"block"`
	NodeCount  int         `json:"node_count"`
}

type GetBlockHashRawReply struct {
	Blockhash    string               `json:"blockhash"`
	SnodeReplies []xrouter.SnodeReply `json:"snodeReplies"`
}

// GetBlockHashRaw
func (service *XRouterService) GetBlockHashRaw(_ *http.Request, args *GetBlockHashRawArgs, reply *GetBlockHashRawReply) error {
	service.log.Info("XRouter: GetBlockHashRaw called")
	if blockhash, snodeReplies, err := service.client.GetBlockHashRaw(args.Blockchain, args.Block, args.NodeCount); err != nil {
		service.log.Fatal("error: %v", err)
		return err
	} else {
		reply.SnodeReplies = snodeReplies
		reply.Blockhash = blockhash
	}
	return nil
}

// GetBlockHashRaw
type GetBlockHashArgs struct {
	Blockchain string `json:"blockchain"`
	Block      string `json:"block"`
	NodeCount  int    `json:"node_count"`
}

type GetBlockHashReply struct {
	SnodeReply xrouter.SnodeReply `json:"snodeReply"`
}

// GetBlockHash
func (service *XRouterService) GetBlockHash(_ *http.Request, args *GetBlockHashArgs, reply *GetBlockHashReply) error {
	service.log.Info("XRouter: GetBlockHash called")
	if snodeReply, err := service.client.GetBlockHash(args.Blockchain, args.Block, args.NodeCount); err != nil {
		service.log.Fatal("error: %v", err)
		return err
	} else {
		reply.SnodeReply = snodeReply
	}
	return nil
}

// GetBlockRaw
type GetBlockRawArgs struct {
	Blockchain string `json:"blockchain"`
	Block      string `json:"block"`
	NodeCount  int    `json:"node_count"`
}

type GetBlockRawReply struct {
	SnodeReplies []xrouter.SnodeReply `json:"snodeReplies"`
	BlockData    string               `json:"blockData"`
}

// GetBlockRaw
func (service *XRouterService) GetBlockRaw(_ *http.Request, args *GetBlockRawArgs, reply *GetBlockRawReply) error {
	service.log.Info("XRouter: GetBlockRaw called")
	if blockData, snodeReplies, err := service.client.GetBlockRaw(args.Blockchain, args.Block, args.NodeCount); err != nil {
		service.log.Fatal("error: %v", err)
		return err
	} else {
		reply.SnodeReplies = snodeReplies
		reply.BlockData = blockData
	}
	return nil
}

// GetBlock
type GetBlockArgs struct {
	Blockchain string `json:"blockchain"`
	Block      string `json:"block"`
	NodeCount  int    `json:"node_count"`
}

type GetBlockReply struct {
	SnodeReply xrouter.SnodeReply `json:"snodeReply"`
}

// GetBlock
func (service *XRouterService) GetBlock(_ *http.Request, args *GetBlockArgs, reply *GetBlockReply) error {
	service.log.Info("XRouter: GetBlock called")
	if snodeReply, err := service.client.GetBlock(args.Blockchain, args.Block, args.NodeCount); err != nil {
		service.log.Fatal("error: %v", err)
		return err
	} else {
		reply.SnodeReply = snodeReply
	}
	return nil
}

// // ShowConfigs
// type ShowConfigsReply struct {
// 	Configs string `json:"configs"`
// }

// // ShowConfigs
// func (service *XRouterService) ShowConfigs(_ *http.Request, _ *struct{}, reply *ShowConfigsReply) error {
// 	service.log.Info("XRouter: ShowConfigs called")
// 	if xrouterReply, err := service.client.ShowConfigs(); err != nil {
// 		service.log.Fatal("error: %v", err)
// 		return err
// 	} else {
// 		reply.Configs =  string(xrouterReply.Reply)
// 	}
// 	return nil
// }

// // Status
// type StatusReply struct {
// 	Status string `json:"Status"`
// }

// // Status
// func (service *XRouterService) Status(_ *http.Request, _ *struct{}, reply *StatusReply) error {
// 	service.log.Info("XRouter: Status called")
// 	if xrouterReply, err := service.client.(); err != nil {
// 		service.log.Fatal("error: %v", err)
// 		return err
// 	} else {
// 		reply.Status =  string(xrouterReply.Reply)
// 	}
// 	return nil
// }
